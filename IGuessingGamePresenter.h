#ifndef IGUESSINGGAMEPRESENTER_H
#define IGUESSINGGAMEPRESENTER_H

class IGuessingGamePresenter
{
  public:
    virtual ~IGuessingGamePresenter() = default;
    virtual void StartRound() = 0;
};

#endif // IGUESSINGGAMEPRESENTER_H
