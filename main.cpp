#include "guessinggame.h"
#include "guessinggamemodel.h"
#include "guessinggamepresenter.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    auto _view = std::make_unique<GuessingGameDialog>();
    auto _model = std::make_unique<GuessingGameModel>();
    auto _presenter = std::make_unique<GuessingGamePresenter>(std::move(_view),
                                                              std::move(_model));
    _presenter->StartGame();
    return a.exec();
}
