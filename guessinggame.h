#ifndef GUESSINGGAME_H
#define GUESSINGGAME_H

#include <QDialog>
#include <QMessageBox>
#include <QMultiMap>

class IGuessingGamePresenter;

QT_BEGIN_NAMESPACE
namespace Ui { class GuessingGameDialog; }
QT_END_NAMESPACE


enum class ExitCode{
  Yes,
  No
};

class GuessingGameDialog : public QDialog
{
    Q_OBJECT

public:
    GuessingGameDialog(QWidget *parent = nullptr);
    ~GuessingGameDialog();
    void SetPresenter(
      IGuessingGamePresenter* _presenter
    );
    ExitCode AskYesNoQuestion(
      const std::string& question
    );

    std::string GetUserInput(
      const std::string& label,
      bool& ok
    );

    void ShowWinMessage(
      const std::string& win
    );

    void StartGame();

private slots:
    void on_okButton_clicked();

    void on_cancelButton_clicked();

private:
    void StartRound();
    IGuessingGamePresenter* presenter;
    Ui::GuessingGameDialog *ui;
};
#endif // GUESSINGGAME_H
