#include "guessinggamepresenter.h"
#include "guessinggamemodel.h"
#include "guessinggame.h"

//--------------------------------------------------------------------------------

GuessingGamePresenter::GuessingGamePresenter(
   std::unique_ptr<GuessingGameDialog> _view,
   std::unique_ptr<GuessingGameModel> _model
) :
   view(std::move(_view)),
   model(std::move(_model))
{
    view->SetPresenter(this);
}

//--------------------------------------------------------------------------------

void GuessingGamePresenter::StartGame()
{
    view->StartGame();
}

//--------------------------------------------------------------------------------

void GuessingGamePresenter::StartRound()
{
    bool startOver = false;
    std::string currentTrait = model->GetFirstInsertedTrait();
    std::string previousTrait = "";
    do
    {
        ExitCode code = view->AskYesNoQuestion("Does the animal that you thought about " + currentTrait);
        if (code == ExitCode::Yes) {
           if (!model->HasDerivedTraits(currentTrait)) {
               CheckAnimal(model->GetAnimal(currentTrait), currentTrait);
               startOver = true;
           } else {
               previousTrait = currentTrait;
               currentTrait = model->GetFirstDerivedTrait(currentTrait);
           }
        } else {
            if (previousTrait.empty()) {
              CheckAnimal("Monkey");
              startOver = true;
            } else {
                std::string nextDerivedTrait = model->GetNextDerivedTrait(previousTrait,
                                                                          currentTrait);
                if (nextDerivedTrait.empty()) {
                    CheckAnimal(model->GetAnimal(previousTrait), previousTrait);
                    startOver = true;
                } else {
                    currentTrait = nextDerivedTrait;
                }
            }
        }
    } while (!startOver);
}

//--------------------------------------------------------------------------------

void GuessingGamePresenter::CheckAnimal(
  const std::string& animal,
  const std::string& animalTrait
)
{
    ExitCode code = view->AskYesNoQuestion("Is the animal that you thought about a " +
                                                               animal);
    if (code == ExitCode::Yes) {
         view->ShowWinMessage(firstWin ? "I win!" : "I win again!");
         firstWin = false;
    } else if (code == ExitCode::No) {
         bool ok;
         std::string newAnimal = view->GetUserInput("What was the animal that you thought about?", ok);

         if (!ok)
             return;

         std::string newTrait = view->GetUserInput("A " + newAnimal + " _________ but a " + animal +
                                         " does not (Fill it with an animal trait, like 'lives in water'", ok);
         if (!ok)
             return;

         model->InsertAnimal(newTrait, newAnimal);
         if (!animalTrait.empty())
            model->InsertDerivedTrait(animalTrait, newTrait);
    }
}

//--------------------------------------------------------------------------------
