#include "guessinggame.h"
#include "ui_guessinggame.h"

#include "IGuessingGamePresenter.h"
#include "QInputDialog"
#include "QMessageBox"

//-----------------------------------------------------------------------------------

GuessingGameDialog::GuessingGameDialog(
  QWidget *parent
) :
  QDialog(parent),
  ui(new Ui::GuessingGameDialog)
{
    ui->setupUi(this);
}

//-----------------------------------------------------------------------------------

GuessingGameDialog::~GuessingGameDialog()
{
    delete ui;
}

//-----------------------------------------------------------------------------------

void GuessingGameDialog::SetPresenter(
  IGuessingGamePresenter* _presenter
)
{
    presenter = _presenter;
}

//-----------------------------------------------------------------------------------

void GuessingGameDialog::on_okButton_clicked()
{
  StartRound();
}

//-----------------------------------------------------------------------------------

void GuessingGameDialog::on_cancelButton_clicked()
{
    close();
}

//-----------------------------------------------------------------------------------

void GuessingGameDialog::StartGame()
{
    show();
}

//-----------------------------------------------------------------------------------

void GuessingGameDialog::StartRound()
{
    hide();
    presenter->StartRound();
    show();
}

//-----------------------------------------------------------------------------------

ExitCode GuessingGameDialog::AskYesNoQuestion(
  const std::string& question
)
{
    QMessageBox messageBox;
    messageBox.setText(QString::fromStdString(question) + "?");
    messageBox.setWindowTitle("Guessing Game");
    messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int result = messageBox.exec();

    ExitCode retCode;

    switch (result) {
        case QMessageBox::Yes:
            retCode = ExitCode::Yes;
            break;
        case QMessageBox::No:
            retCode = ExitCode::No;
            break;
        default:
            retCode = ExitCode::No;
    }

    return retCode;
}

//-----------------------------------------------------------------------------------

std::string GuessingGameDialog::GetUserInput(
  const std::string& label,
  bool& ok
)
{
    return QInputDialog::getText(this, "Guessing Game",
                                 QString::fromStdString(label), QLineEdit::Normal,
                                 QString(), &ok).toStdString();
}

//-----------------------------------------------------------------------------------

void GuessingGameDialog::ShowWinMessage(
  const std::string& winMsg
)
{
    QMessageBox::information(this, "Guessing Game",
                             QString::fromStdString(winMsg), QMessageBox::Ok);
}

//-----------------------------------------------------------------------------------
