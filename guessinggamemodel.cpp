#include "guessinggamemodel.h"

//---------------------------------------------------------------------------

GuessingGameModel::GuessingGameModel()
{
    InsertAnimal("lives in water", "shark");
}

//---------------------------------------------------------------------------

void GuessingGameModel::InsertAnimal(
  const std::string& trait,
  const std::string& animal
)
{
    traits_animals.insert(trait, animal);
}

//---------------------------------------------------------------------------

void GuessingGameModel::InsertDerivedTrait(
  const std::string& trait,
  const std::string& derivedTrait
)
{
    traits_derivedTraits.insert(trait, derivedTrait);
}

//---------------------------------------------------------------------------

std::string GuessingGameModel::GetAnimal(
  const std::string& trait
)
{
    return traits_animals.value(trait, "");
}

//---------------------------------------------------------------------------

std::string GuessingGameModel::GetFirstInsertedTrait() const
{

    return "lives in water";
}

//---------------------------------------------------------------------------

bool GuessingGameModel::HasDerivedTraits(
  const std::string& trait
)
{
    return !traits_derivedTraits.values(trait).empty();
}

//---------------------------------------------------------------------------

std::string GuessingGameModel::GetFirstDerivedTrait(
  const std::string& trait
)
{
    std::string firstDerived;

    QList<std::string> derivedTraits = traits_derivedTraits.values(trait);
    if (!derivedTraits.empty())
        firstDerived = derivedTraits.last();

    return firstDerived;
}

//---------------------------------------------------------------------------

std::string GuessingGameModel::GetNextDerivedTrait(
  const std::string& ownerTrait,
  const std::string& derivedTrait
)
{
    std::string newDerivedTrait;
    QList<std::string> derivedTraits = traits_derivedTraits.values(ownerTrait);
    int new_index = derivedTraits.indexOf(derivedTrait) - 1;
    if (new_index >= 0) {
        newDerivedTrait = derivedTraits.at(new_index);
    } else {
        newDerivedTrait = "";
    }

    return newDerivedTrait;
}

//---------------------------------------------------------------------------
