#ifndef GUESSINGGAMEPRESENTER_H
#define GUESSINGGAMEPRESENTER_H

#include "IGuessingGamePresenter.h"
#include <string>
#include <memory>

//---------------------------------------------------------------------

class GuessingGameDialog;
class GuessingGameModel;

//---------------------------------------------------------------------

class GuessingGamePresenter : public IGuessingGamePresenter
{
  public:
    GuessingGamePresenter(
        std::unique_ptr<GuessingGameDialog> _view,
        std::unique_ptr<GuessingGameModel> _model
    );

    ~GuessingGamePresenter() = default;

    void StartGame();
    void StartRound() override;

  private:
    void CheckAnimal(
      const std::string& animal,
      const std::string& animalTrait = ""
    );

    std::unique_ptr<GuessingGameDialog> view;

    std::unique_ptr<GuessingGameModel> model;

    bool firstWin = true;
};

#endif // GUESSINGGAMEPRESENTER_H
