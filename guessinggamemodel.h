#ifndef GUESSINGGAMEMODEL_H
#define GUESSINGGAMEMODEL_H

#include <QMultiMap>
#include <string>

class GuessingGameModel
{
  public:
    GuessingGameModel();
    ~GuessingGameModel() = default;

    void InsertAnimal(
      const std::string& animal,
      const std::string& trait
    );

    void InsertDerivedTrait(
      const std::string& trait,
      const std::string& derivedTrait
    );

    QMultiMap<std::string, std::string> GetDerivedTraits(
      const std::string& trait
    );

    std::string GetAnimal(
      const std::string& trait
    );

    std::string GetFirstInsertedTrait() const;

    bool HasDerivedTraits(
      const std::string& trait
    );

    std::string GetFirstDerivedTrait(
      const std::string& trait
    );

    std::string GetNextDerivedTrait(
      const std::string& ownerTrait,
      const std::string& derivedTrait
    );

  private:
    QMultiMap<std::string, std::string> traits_animals;
    QMultiMap<std::string, std::string> traits_derivedTraits;
};

#endif // GUESSINGGAMEMODEL_H
